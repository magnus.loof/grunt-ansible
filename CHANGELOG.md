# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.3.1] - 2020-02-04
### Fixed
- The command was shown, but not run.

## [0.3.0] - 2020-02-04
### Added
- Global configuration is supported under key `options`, similar to other grunt plugins.
- Support for running `ansible-galaxy` using `grunt ansible:galaxy`.

### Changed
- Command line args is flattened down into same level as `playbook`.
- Configuration is broken up into `playbooks` and `galaxy`.

## [0.2.0] - 2020-01-13
### Added
- Allow setting playbook path explicitly in task options.

## [0.1.0] - 2018-09-13
### Added
- Specify Ansible CLI arguments via either the config or as flags passed to grunt.

[0.3.1]: https://gitlab.com/basalt/grunt-ansible/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.com/basalt/grunt-ansible/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/basalt/grunt-ansible/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/basalt/grunt-ansible/compare/8c31edc...0.1.0
