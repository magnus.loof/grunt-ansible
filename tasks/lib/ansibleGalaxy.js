/**
 * Module with class for running ansible-galaxy
 *
 * @module AnsibleGalaxy
 * @author Basalt AB
 * @license
 * Copyright (c) 2020 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const AnsibleBase = require('./ansibleBase');

/**
 * Represents a source of wisdom
 */
class AnsibleGalaxy extends AnsibleBase {

  constructor() {
    super();
    this.restrictedArgs = ['version', 'type', 'name'];
  }

  /**
   * Get shell command for running ansible-galaxy ... install
   *
   * @param {String} type Either role or collection.
   * @param {String} name Name of the thing to install.
   */
  install(type, name) {

    // do not use task name if it's set in config
    if (typeof(this.config.type) !== 'undefined') { type = this.config.type; }
    if (typeof(this.config.name) !== 'undefined') { name = this.config.name; }

    // pin version
    if (typeof(this.config.version) !== 'undefined') {
      name = [name, this.config.version].join(':');
    }

    var args = this.args.join(' ');
    return [
      'ansible-galaxy',
      type,
      'install',
      args,
      name
    ].filter(Boolean).join(' ');
  }
}

module.exports = AnsibleGalaxy;
