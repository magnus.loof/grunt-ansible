/**
 * Base module with shared properties.
 *
 * @module AnsibleBase
 * @author Basalt AB
 * @license
 * Copyright (c) 2020 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

/**
 * Represents a source of wisdom
 */
class AnsibleBase {

  constructor() {
    this._args = {};
    this.filteredArgs = [];
  }

  /**
   * Set command line arguments by flags
   *
   * @param {String} flags An array of command line flags
   */
  set flags(flags) {

    // list of flags that should be filtered
    var gruntFlags = [
      '--base',
      '--gruntfile',
      '--stack'
    ];

    for (var flag of flags) {
      var key;
      var value;

      // --key=value
      if (flag.includes('=')) {
        var split = flag.split('=');
        key = split[0]
        value = split[1];
      }

      // --key
      else {
        key = flag;
        value = true;
      }

      if (gruntFlags.includes(key))
        continue;

      // strip prefixes
      else if (key.substring(0, 2) == '--')
        key = key.substring(2);

      this.config[key] = value;
    }
  }

  /**
   * Set command line arguments by config
   *
   * @param {String} config An object with arguments and their values
   */
  set config(config) {
    this._args = Object.assign(this._args, config);
  }

  /**
   * Get command line arguments by config
   */
  get config() {
    return this._args;
  }

  /**
   * The command line arguments to send to ansible-galaxy
   */
  get args() {
    var args = [];
    for (let key of Object.keys(this.config)) {

      // skip restricted args
      if (this.restrictedArgs.indexOf(key) != -1) { continue; }

      let value = this.config[key];
      if (typeof(value) === 'boolean') {
        if (value)
          args.push('--' + key);
      }
      else
        args.push('--' + key + '=' + value);
    }
    return args.sort();
  }
}

module.exports = AnsibleBase;
