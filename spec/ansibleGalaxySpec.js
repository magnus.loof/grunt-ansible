'use strict';

describe("AnsibleGalaxy", function() {
  var ansible;
  var AnsibleGalaxy = require('../tasks/lib/ansibleGalaxy.js');

  beforeEach(function() {
    ansible = new AnsibleGalaxy();
  });

  describe(".install", function() {

    it("should install collection", function() {
      expect(ansible.install('collection', 'foo'))
        .toEqual('ansible-galaxy collection install foo');
    });

    it("should install role", function() {
      expect(ansible.install('role', 'foo'))
        .toEqual('ansible-galaxy role install foo');
    });

    it("should add arguments", function() {
      ansible.config = { server: 'galaxy.example.com', force: true };
      expect(ansible.install('collection', 'foo'))
        .toEqual('ansible-galaxy collection install --force --server=galaxy.example.com foo');
    });

    it("should add version", function() {
      ansible.config = { version: '1.2.3' };
      expect(ansible.install('collection', 'foo'))
        .toEqual('ansible-galaxy collection install foo:1.2.3');
    });

  });

  describe(".args", function() {
    it("should filter grunt flags", function() {
      ansible.flags = ['--stack', '--base', '--gruntfile=test', '--user=bob'];
      expect(ansible.args).toEqual(['--user=bob']);
    });
  });

  describe(".config", function() {
    it("should generate command line args", function() {
      ansible.config = { server: 'galaxy.example.com', force: true };
      expect(ansible.args).toEqual(['--force', '--server=galaxy.example.com']);
    });

    it("should skip false values", function() {
      ansible.config = { server: 'galaxy.example.com', force: false };
      expect(ansible.args).toEqual(['--server=galaxy.example.com']);
    });
  });

});
