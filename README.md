# grunt-ansible

Grunt plugin for running Ansible. Supports both `ansible-playbook` and
`ansible-galaxy`.

## Getting Started

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out
the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains
how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as
install and use Grunt plugins. Once you're familiar with that process, you may
install this plugin with this command:

```shell
npm install grunt-ansible --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with
this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-ansible');
```

## Ansible task

Run playbooks with the `grunt ansible` or `grunt ansible:playbook` command.

Download content from Ansible Galaxy with the `grunt ansible:galaxy` command.

Task targets, files and options may be specified according to the grunt
[Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### Options

The options are divided into two groups: `playbooks` and `galaxy`. The first
group is used for `grunt ansible:playbook` and the other for
`grunt ansible:galaxy`.

All options are parsed into command line options except a few reserved keys.

For example:

```js
my_playbook: {
  playbook: 'path/to/playbook',
  arg1: 'foo',
  arg2: 'bar'
}
```

...would translate `arg1` and `arg2` into command line arguments, while using
the reserved `playbook` as the path to the playbook to run.

Options can be specified either globally under the key `options` or per item:

```js
options: {
  inventory: 'default-inventory'
},
my_playbook: {
  playbook: 'path/to/playbook',
  inventory: 'inventory-for-my-playbook'
}
```

#### playbooks

##### playbook

Type: `String`

Path to the playbook to run. If not given then the name of the task will be
used instead.

#### galaxy

##### version

Type: `String`

The version to install. By default the latest version is installed (nothing
specified).

##### type

Type: `String`

The type to install. Can be either `role` or `collection`. Defaults to
`collection`.

## Example

Each target should correspond to a playbook. The options under each target
is turned into command line arguments. For example:

```js
ansible: {
  playbooks: {
    options: {
      timeout: 10
    },
    playbook_1: {
      become: true,
      user: 'bob',
      inventory: '/etc/hosts'
    },
    playbook_2: {
      playbook: 'path/to/playbook.yml',
      inventory: 'localhost,'
      connection: local
    }
  },
  galaxy: {
    options: {
      verbose: true
    },
    awx: {
      collections-path: 'path/to/collection',
      version: '9.1.1',
      name: 'awx.awx'
    },
    docker: {
      server: 'galaxy.example.com',
      type: 'role',
      name: 'example.docker'
    }
  },
}
```

The playbooks can then be run with:

```shell
$ grunt ansible:playbook_1
# ansible-playbook --timeout=10 --become --user=bob playbook_1.yml

$ grunt ansible:playbook:playbook_2
# ansible-playbook --timeout=10 --connection=local path/to/playbook.yml

$ grunt ansible:galaxy:awx
# ansible-galaxy collection install --verbose \
#   --collections-path=path/to/collection \
#   awx.awx:9.1.1

$ grunt ansible:galaxy:docker
# ansible-galaxy role install --verbose \
#   --server=galaxy.example.com \
#   example.awx:9.1.1
```

It is also possible to send arguments via the command line like so:

```shell
$ grunt ansible:playbook.yaml --become --user=bob
# ansible-playbook --timeout=10 --become --user=bob playbook.yaml
```
